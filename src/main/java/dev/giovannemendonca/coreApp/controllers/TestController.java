package dev.giovannemendonca.coreApp.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin-tools")
public class TestController {


    @GetMapping("/status")
    public String status(){
        return "API is running version 1.0";
    }
}
