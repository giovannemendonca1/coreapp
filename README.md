# Projeto CI/CD com GitLab

Este projeto consiste em um exemplo de configuração de um pipeline CI/CD utilizando o GitLab CI/CD para automatizar os estágios de teste, build e deploy de uma aplicação Java.

## Funcionalidades

- **Testes Automatizados**: O pipeline inclui um estágio de testes automatizados utilizando o Maven para executar os testes unitários da aplicação.

- **Build da Aplicação**: Após os testes, o pipeline realiza o build da aplicação utilizando o Maven e gera o arquivo .war necessário para implantação.

- **Implantação Automatizada**: Uma vez que a aplicação é construída com sucesso, o pipeline utiliza SSH para implantar o arquivo .war gerado em um servidor remoto. Dependendo do branch em que a alteração foi feita, o deploy é direcionado para o ambiente de produção ou de stage.

## Estrutura do Projeto

O projeto possui a seguinte estrutura de arquivos:

- **.gitlab-ci.yml**: Este arquivo contém a configuração do pipeline CI/CD. Ele define os estágios, as tarefas a serem executadas em cada estágio e as condições de execução.

- **src/**: Diretório contendo o código-fonte da aplicação Java.

## Requisitos

Para executar o pipeline CI/CD deste projeto, é necessário ter:

- Acesso a um ambiente GitLab com funcionalidade CI/CD habilitada.
- Configuração das variáveis de ambiente necessárias no GitLab para as credenciais de implantação no servidor remoto.

## Configuração

Antes de executar o pipeline CI/CD, é necessário configurar as seguintes variáveis de ambiente no GitLab:

- `PRIVATE_KEY_PROD`: Chave privada SSH para acessar o servidor de produção.
- `USER_PROD`: Nome de usuário para acessar o servidor de produção.
- `SERVER_HOST_PROD`: Endereço do servidor de produção.
- `PATH_DEPLOY_PROD`: Caminho no servidor de produção onde o arquivo .war será implantado.

- `PRIVATE_KEY_STAGE`: Chave privada SSH para acessar o servidor de stage.
- `USER_STAGE`: Nome de usuário para acessar o servidor de stage.
- `SERVER_HOST_STAGE`: Endereço do servidor de stage.
- `PATH_DEPLOY_STAGE`: Caminho no servidor de stage onde o arquivo .war será implantado.

Certifique-se de que as variáveis estejam configuradas corretamente e protegidas para evitar exposição de informações sensíveis.

## Configuração das Máquinas na GCP

Este projeto utiliza duas máquinas virtuais na Google Cloud Platform (GCP) para realizar o deploy da aplicação. Dentro de cada máquina, foi configurado um servidor Tomcat para hospedar a aplicação. Durante a fase de build, é gerado um arquivo .war que será implantado no servidor Tomcat correspondente, de acordo com o branch em que a alteração foi feita.

## Execução do Pipeline

O pipeline CI/CD é composto por três estágios:

1. **Test**: Neste estágio, os testes automatizados da aplicação são executados utilizando o Maven. Este estágio é acionado automaticamente quando há alterações no branch `development`.

2. **Build**: Após a conclusão dos testes, a aplicação é compilada e empacotada em um arquivo .war utilizando o Maven. Este estágio é acionado automaticamente quando há alterações no branch `main` ou `staging`.

3. **Deploy**: Uma vez que o arquivo .war é gerado com sucesso, ele é implantado no servidor Tomcat correspondente (produção ou stage) utilizando SSH. este estágio é acionado automaticamente quando há alterações no branch `main` ou `staging`.

## Contribuição para o Aprendizado

Este projeto oferece uma valiosa oportunidade de aprendizado ao permitir que você ganhe experiência prática com:

- Configuração e automação de pipelines CI/CD utilizando o GitLab CI/CD.
- Utilização de ferramentas como Maven, GitLab, SSH e Google Cloud Platform.
- Implementação de estratégias de deploy automatizado para diferentes ambientes (produção e stage).
- Integração de testes automatizados no processo de construção e deploy da aplicação.
- Configuração de máquinas virtuais na Google Cloud Platform.
- Configuração de servidores Tomcat para hospedar aplicações Java.
- Resolução de problemas e depuração de erros em pipelines CI/CD.
- Configuração de permissões dentro da máquina virtual para que o deploy seja feito corretamente.

A compreensão e a prática desses conceitos são fundamentais para qualquer desenvolvedor moderno e podem ser aplicadas em uma variedade de contextos profissionais.

